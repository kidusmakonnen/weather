package com.kidus.androidapps.weather;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Kidus on .
 */
public class Weather implements Serializable {
    private City mCity;
    private double mLatitude, mLongitude;
    private int mOffset;
    private WeatherData mCurrentWeatherData;
    private ArrayList<WeatherDataDaily> mDailyWeatherData;
    private ArrayList<WeatherData> mHourlyWeatherData;
    private String mJsonResponse;

    public String getJsonResponse() {
        return mJsonResponse;
    }

    public void setJsonResponse(String jsonResponse) {
        mJsonResponse = jsonResponse;
    }

    private Context mContext;

    public Weather(Context context, City city) {
        mCurrentWeatherData = null;
        mDailyWeatherData = null;
        mHourlyWeatherData = null;
        mContext = context;
        mCity = city;
    }

    public Weather(double latitude, double longitude, int offset) {
        mLatitude = latitude;
        mLongitude = longitude;
        mOffset = offset;
        mCurrentWeatherData = null;
        mDailyWeatherData = null;
        mHourlyWeatherData = null;
    }

    public void prepareFromJSON(String jsonString) {
        setJsonResponse(jsonString);
        JSONObject tempJsonObject = JSONParse.toJSONObject(jsonString);
        mCurrentWeatherData = getCurrentWeather((JSONObject) JSONParse.get(tempJsonObject, "currently"));
//        mDailyWeatherData = generateWeatherDataArray((JSONObject) JSONParse.get(tempJsonObject, "daily"));
        mDailyWeatherData = generateWeatherDataDailyArray((JSONObject) JSONParse.get(tempJsonObject, "daily"));
        mHourlyWeatherData = generateWeatherDataArray((JSONObject) JSONParse.get(tempJsonObject, "hourly"));//get back here if shits go berzerk
    }

    private WeatherData getCurrentWeather(JSONObject jsonObject) {
        return generateWeatherData(jsonObject);
    }

    private ArrayList<WeatherData> generateWeatherDataArray(JSONObject jsonObject) {
        ArrayList<WeatherData> weatherData = new ArrayList<>();
        JSONArray tempJsonArray = JSONParse.getJSONArray(jsonObject, "data");
        for (int i = 0; i < tempJsonArray.length(); i++) {
            weatherData.add(generateWeatherData(JSONParse.getJSONObject(tempJsonArray, i)));
        }
        return weatherData;
    }

    private ArrayList<WeatherDataDaily> generateWeatherDataDailyArray(JSONObject jsonObject) {
        ArrayList<WeatherDataDaily> weatherData = new ArrayList<>();
        JSONArray tempJsonArray = JSONParse.getJSONArray(jsonObject, "data");
        for (int i = 0; i < tempJsonArray.length(); i++) {
            weatherData.add(generateWeatherDataDaily(JSONParse.getJSONObject(tempJsonArray, i)));
        }
        return weatherData;
    }

    private WeatherData generateWeatherData(JSONObject object) {
        return new WeatherData(
                mContext,
                Long.parseLong(JSONParse.get(object, "time").toString()),
                getWeatherIcon(JSONParse.get(object, "icon").toString()),
                JSONParse.get(object, "summary").toString(),
                Double.parseDouble(JSONParse.get(object, "precipIntensity").toString()),
                Double.parseDouble(JSONParse.get(object, "precipProbability").toString()),
                JSONParse.get(object, "precipType").toString(),
                Double.parseDouble(JSONParse.get(object, "temperature").toString()),
                Double.parseDouble(JSONParse.get(object, "apparentTemperature").toString()),
                Double.parseDouble(JSONParse.get(object, "dewPoint").toString()),
                Double.parseDouble(JSONParse.get(object, "humidity").toString()),
                Double.parseDouble(JSONParse.get(object, "windSpeed").toString()),
                Double.parseDouble(JSONParse.get(object, "windBearing").toString()),
                Double.parseDouble(JSONParse.get(object, "cloudCover").toString()),
                Double.parseDouble(JSONParse.get(object, "pressure").toString()),
                Double.parseDouble(JSONParse.get(object, "ozone").toString())
        );
    }

    private WeatherDataDaily generateWeatherDataDaily(JSONObject object) {
        return new WeatherDataDaily(
                mContext,
                Long.parseLong(JSONParse.get(object, "time").toString()),
                getWeatherIcon(JSONParse.get(object, "icon").toString()),
                JSONParse.get(object, "summary").toString(),
                Double.parseDouble(JSONParse.get(object, "precipIntensity").toString()),
                Double.parseDouble(JSONParse.get(object, "precipProbability").toString()),
                JSONParse.get(object, "precipType").toString(),
                Double.parseDouble(JSONParse.get(object, "dewPoint").toString()),
                Double.parseDouble(JSONParse.get(object, "humidity").toString()),
                Double.parseDouble(JSONParse.get(object, "windSpeed").toString()),
                Double.parseDouble(JSONParse.get(object, "windBearing").toString()),
                Double.parseDouble(JSONParse.get(object, "cloudCover").toString()),
                Double.parseDouble(JSONParse.get(object, "pressure").toString()),
                Double.parseDouble(JSONParse.get(object, "ozone").toString()),
                Long.parseLong(JSONParse.get(object, "sunriseTime").toString()),
                Long.parseLong(JSONParse.get(object, "sunsetTime").toString()),
                Long.parseLong(JSONParse.get(object, "temperatureMinTime").toString()),
                Long.parseLong(JSONParse.get(object, "temperatureMaxTime").toString()),
                Long.parseLong(JSONParse.get(object, "apparentTemperatureMinTime").toString()),
                Long.parseLong(JSONParse.get(object, "apparentTemperatureMaxTime").toString()),
                Double.parseDouble(JSONParse.get(object, "moonPhase").toString()),
                Double.parseDouble(JSONParse.get(object, "precipIntensityMax").toString()),
                Double.parseDouble(JSONParse.get(object, "temperatureMin").toString()),
                Double.parseDouble(JSONParse.get(object, "temperatureMax").toString()),
                Double.parseDouble(JSONParse.get(object, "apparentTemperatureMin").toString()),
                Double.parseDouble(JSONParse.get(object, "apparentTemperatureMax").toString())
        );
    }

    private WeatherData.WeatherIcon getWeatherIcon(String s) {
        switch (s) {
            case "clear-day":
                return WeatherData.WeatherIcon.CLEAR_DAY;
            case "clear-night":
                return WeatherData.WeatherIcon.CLEAR_NIGHT;
            case "rain":
                return WeatherData.WeatherIcon.RAIN;
            case "snow":
                return WeatherData.WeatherIcon.SNOW;
            case "sleet":
                return WeatherData.WeatherIcon.SLEET;
            case "wind":
                return WeatherData.WeatherIcon.WIND;
            case "fog":
                return WeatherData.WeatherIcon.FOG;
            case "cloudy":
                return WeatherData.WeatherIcon.CLOUDY;
            case "partly-cloudy-night":
                return WeatherData.WeatherIcon.PARTLY_CLOUDY_NIGHT;
            case "partly-cloudy-day":
                return WeatherData.WeatherIcon.PARTLY_CLOUDY_DAY;
            case "hail":
                return WeatherData.WeatherIcon.HAIL;
            case "thunderstorm":
                return WeatherData.WeatherIcon.THUNDERSTORM;
            case "tornado":
                return WeatherData.WeatherIcon.TORNADO;
            default:
                return null;
        }
    }

    public static ArrayList<Weather> prepareWeatherListFromCityArray(ArrayList<City> cities) {
        ArrayList<Weather> weathers = new ArrayList<>();
        for (int i = 0; i < cities.size(); i++) {
            weathers.add(new Weather(null, cities.get(i)));
        }
        return weathers;
    }



    public City getCity() {
        return mCity;
    }

    public String  getCityName() {
        return mCity.getName();
    }

    public String getCityCountry() {
        return mCity.getCountryName();
    }

    public double getLatitude() {
        return mLatitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public int getOffset() {
        return mOffset;
    }

    public WeatherData getCurrentWeatherData() {
        return mCurrentWeatherData;
    }

    public ArrayList<WeatherDataDaily> getDailyWeatherData() {
        return mDailyWeatherData;
    }

    public ArrayList<WeatherData> getHourlyWeatherData() {
        return mHourlyWeatherData;
    }


}
