package com.kidus.androidapps.weather;

import java.util.ArrayList;

/**
 * Created by Kidus on .
 */
public class CityList {
    public static ArrayList<City> sCities = new ArrayList<>();

    public static void addToCityList(City city) {
        sCities.add(city);
    }
    //    public static City[] cities = {
    static {
        sCities.add(new City(LocationName.get("Addis Ababa", "አዲስ አበባ"), LocationName.get("Ethiopia", "ኢትዮጵያ"), 8.9806, 38.7578, R.drawable.addis, R.drawable.addis_night));
        sCities.add(new City(LocationName.get("Adama", "አዳማ"), LocationName.get("Ethiopia", "ኢትዮጵያ"), 8.5263, 39.2583, R.drawable.adama_day, R.drawable.addis));
        sCities.add(new City(LocationName.get("Dire Dawa", "ድሬደዋ"), LocationName.get("Ethiopia", "ኢትዮጵያ"), 9.6009, 41.8501, R.drawable.dire_dawa_day, R.drawable.addis));
        sCities.add(new City(LocationName.get("Awassa", "አዋሳ"), LocationName.get("Ethiopia", "ኢትዮጵያ"), 7.0504, 38.4955, R.drawable.hawassa_day, R.drawable.addis));
        sCities.add(new City(LocationName.get("Mekelle", "መቀሌ"), LocationName.get("Ethiopia", "ኢትዮጵያ"), 13.4936, 39.4657, R.drawable.mekelle_day, R.drawable.addis));
        sCities.add(new City(LocationName.get("Jijiga", "ጅጅጋ"), LocationName.get("Ethiopia", "ኢትዮጵያ"), 9.3568, 42.7955, R.drawable.jijiga_day, R.drawable.addis));
        sCities.add(new City(LocationName.get("Ottawa", "ኦታዋ"), LocationName.get("Canada", "ካናዳ"), 45.4215, -75.6972, R.drawable.ottawa_day, R.drawable.addis));
        sCities.add(new City(LocationName.get("Mumbai", "ሙምባይ"), LocationName.get("India", "ሕንድ"), 19.0760, 72.8777, R.drawable.mumbai_day, R.drawable.addis));
        sCities.add(new City(LocationName.get("New York", "ኒው ዮርክ"), LocationName.get("US", "ዩ.ኤስ."), 40.7128, -74.0059, R.drawable.newyork_day, R.drawable.addis));
        sCities.add(new City(LocationName.get("London", "ለንደን"), LocationName.get("UK", "ዩኬ"), 51.5074, -0.1278, R.drawable.london_day, R.drawable.addis));
    }


}
