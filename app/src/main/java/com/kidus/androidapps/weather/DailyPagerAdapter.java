package com.kidus.androidapps.weather;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by Kidus on .
 */
public class DailyPagerAdapter extends PagerAdapter {

    private Context mContext;
    private Weather mWeather;
    private LayoutInflater mInflater;

    public DailyPagerAdapter(Context context, Weather weather) {
        mContext = context;
        mWeather = weather;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        TextView dailyTitleTextView, dailySubtitleTextView;
        ImageView dailyIcon;

        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = mInflater.inflate(R.layout.daily_hourly_items, container, false);

        dailySubtitleTextView = (TextView) itemView.findViewById(R.id.daily_hourly_subtitle_text_view);
        dailyTitleTextView = (TextView) itemView.findViewById(R.id.daily_hourly_title_text_view);
        dailyIcon = (ImageView) itemView.findViewById(R.id.daily_hourly_icon_image_view);

        WeatherDataDaily dailyWeatherData = mWeather.getDailyWeatherData().get(position);

        dailyTitleTextView.setText(dailyWeatherData.getFormattedDate("EEEE"));
        dailySubtitleTextView.setText(
                String.format("%dº/%dº", (int) dailyWeatherData.getTemperatureMin(), (int) dailyWeatherData.getTemperatureMax())
        );

        Picasso.with(mContext).load(dailyWeatherData.getIconResourceId()).into(dailyIcon);

        container.addView(itemView);

        return itemView;
    }

    @Override
    public int getCount() {
        return mWeather.getDailyWeatherData().size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view ==  object;
    }

    @Override
    public float getPageWidth(int position) {
        return 1/3f;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}
