package com.kidus.androidapps.weather;

import android.content.Context;
import android.location.GpsStatus;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kidus on .
 */
public class CityListWeatherAdapter extends RecyclerView.Adapter<CityListWeatherAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<Weather> mWeathers;
    private Listener mListener;

    interface Listener {
        void onClick(int position);
        void onLongClick(int position, View v);
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    public CityListWeatherAdapter(Context context, ArrayList<Weather> weathers) {
        mContext = context;
        mWeathers = weathers;
    }

    @Override
    public CityListWeatherAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.city_weather_card_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CityListWeatherAdapter.ViewHolder holder, final int position) {
        Weather weather = mWeathers.get(position);
        WeatherDataDaily dailyWeather = weather.getDailyWeatherData().get(0);
        holder.mCityNameTextView.setText(weather.getCityName());
        holder.mCityWeatherSummaryTextView.setText(weather.getCurrentWeatherData().getSummary());
        holder.mCityWeatherLoTextView.setText(String.valueOf((int) dailyWeather.getTemperatureMin()));
        holder.mCityWeatherHiTextView.setText(String.valueOf((int) dailyWeather.getTemperatureMax()));
//        holder.mCityWeatherIconImageView.setImageResource(weather.getCurrentWeatherData().getIconResourceId());
        Picasso.with(mContext).load(weather.getCurrentWeatherData().getIconResourceId()).into(holder.mCityWeatherIconImageView);
        holder.mCountryNameTextView.setText(weather.getCityCountry());
        holder.mCityCurrentTemperatureTextView.setText(
                String.valueOf((int) weather.getCurrentWeatherData().getTemperature())
        );

        holder.mCityCoverLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onClick(position);
                }
            }
        });

        holder.mCityCoverLinearLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mListener != null) {
                    mListener.onLongClick(position, holder.mCityCoverLinearLayout);
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mWeathers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mCityNameTextView, mCountryNameTextView;
        private TextView mCityWeatherSummaryTextView;
        private TextView mCityWeatherHiTextView, mCityWeatherLoTextView;
        private TextView mCityCurrentTemperatureTextView;
        private ImageView mCityWeatherIconImageView;
        private LinearLayout mCityCoverLinearLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            mCityNameTextView = (TextView) itemView.findViewById(R.id.city_name_card_text_view);
            mCountryNameTextView = (TextView) itemView.findViewById(R.id.city_country_name_card_text_view);
            mCityWeatherSummaryTextView = (TextView) itemView.findViewById(R.id.city_current_summary_text_view);
            mCityWeatherIconImageView = (ImageView) itemView.findViewById(R.id.city_card_icon_image_view);
            mCityWeatherHiTextView = (TextView) itemView.findViewById(R.id.city_card_hi_text_view);
            mCityWeatherLoTextView = (TextView) itemView.findViewById(R.id.city_card_lo_text_view);
            mCityCurrentTemperatureTextView = (TextView) itemView.findViewById(R.id.city_current_temperature_card_text_view);
            mCityCoverLinearLayout = (LinearLayout) itemView.findViewById(R.id.city_weather_cover_linear_layout);
        }
    }
}
