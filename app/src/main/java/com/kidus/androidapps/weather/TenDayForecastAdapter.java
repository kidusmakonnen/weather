package com.kidus.androidapps.weather;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Kidus on .
 */
public class TenDayForecastAdapter extends RecyclerView.Adapter<TenDayForecastAdapter.ViewHolder> {
    Weather mWeather;

    public TenDayForecastAdapter(Weather weather) {
        mWeather = weather;
    }

    @Override
    public TenDayForecastAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.weather_card_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TenDayForecastAdapter.ViewHolder holder, int position) {
        WeatherDataDaily weatherData = mWeather.getDailyWeatherData().get(position);
        String day = weatherData.getFormattedDate("EE").toUpperCase();
        String summary = weatherData.getSummary().toLowerCase();
        summary = summary.substring(0, summary.length() - 1);//get rid of the period at the end of the sentence

        String hi = String.valueOf((int) weatherData.getTemperatureMax());
        String lo = String.valueOf((int) weatherData.getTemperatureMin());

        holder.summaryTextView.setText(summary);
        holder.dayTextView.setText(day);
        holder.hiTextView.setText(hi);
        holder.loTextView.setText(lo);
        holder.iconImageView.setImageResource(weatherData.getIconResourceId());
    }

    @Override
    public int getItemCount() {
        return mWeather.getDailyWeatherData().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView dayTextView, summaryTextView, hiTextView, loTextView;
        ImageView iconImageView;
        public ViewHolder(View itemView) {
            super(itemView);
            dayTextView = (TextView) itemView.findViewById(R.id.weather_card_day_text_view);
            summaryTextView = (TextView) itemView.findViewById(R.id.weather_card_summary_text_view);
            hiTextView = (TextView) itemView.findViewById(R.id.weather_card_hi_text_view);
            loTextView = (TextView) itemView.findViewById(R.id.weather_card_lo_text_view);
            iconImageView = (ImageView) itemView.findViewById(R.id.weather_card_icon_image_view);
        }
    }
}
