package com.kidus.androidapps.weather;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Kidus on .
 */
public class JSONParse {
    public static JSONObject toJSONObject(String json) {
        JSONObject temp = null;
        try {
            temp = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return temp;
    }

    public static Object get(JSONObject object, String key) {
        Object response = null;
        try {
            response = object.get(key);
        } catch (JSONException e) {
            response = "";
            e.printStackTrace();
        }
        return response;
    }

    public static JSONArray getJSONArray(JSONObject object, String key) {
        JSONArray jsonArray = null;
        try {
            jsonArray = object.getJSONArray(key);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    public static JSONObject getJSONObject(JSONArray jsonArray, int index) {
        JSONObject jsonObject = null;
        try {
            jsonObject = jsonArray.getJSONObject(index);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
