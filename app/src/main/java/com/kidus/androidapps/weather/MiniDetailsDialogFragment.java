package com.kidus.androidapps.weather;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by Kidus on .
 */
public class MiniDetailsDialogFragment extends DialogFragment {
    private ViewPager mHourlyViewPager, mDailyViewPager;
    private PagerAdapter mHourlyPagerAdapter, mDailyPagerAdapter;
    private TextView mLocationNameTextView;
    private TextView mHiTempTextView, mLoTempTextView;
    private TextView mForecastTextView;
    private ImageView mWeatherIconImageView;

    private ImageView mShareImageView;

    private RelativeLayout mBadgeRelativeLayout, mDetailsRelativeLayout;

    private Weather mWeather;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.weather_mini_details, container, false);

        mWeather = (Weather) getArguments().getSerializable("WEATHER");
        WeatherData currentWeatherData = mWeather.getCurrentWeatherData();
        WeatherDataDaily todayWeather = mWeather.getDailyWeatherData().get(0);

        mLocationNameTextView = (TextView) view.findViewById(R.id.city_country_temp_mini_details_text_view);
        mLocationNameTextView.setText(
                String.format("%s, %s | %dº",
                        mWeather.getCityName(), mWeather.getCityCountry(), (int) currentWeatherData.getTemperature())
        );

        mHiTempTextView = (TextView) view.findViewById(R.id.city_card_hi_mini_details_text_view);
        mHiTempTextView.setText(
                String.valueOf((int) todayWeather.getTemperatureMax())
        );

        mLoTempTextView = (TextView) view.findViewById(R.id.city_card_lo_mini_details_text_view);
        mLoTempTextView.setText(
                String.valueOf((int) todayWeather.getTemperatureMin())
        );

        mWeatherIconImageView = (ImageView) view.findViewById(R.id.city_card_icon_mini_details_image_view);
        Picasso.with(getContext()).load(todayWeather.getIconResourceId()).into(mWeatherIconImageView);

        mForecastTextView = (TextView) view.findViewById(R.id.forecast_mini_details_text_view);
        mForecastTextView.setText(todayWeather.getSummary());


        mShareImageView = (ImageView) view.findViewById(R.id.share_text_view);
        mShareImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mBadgeRelativeLayout.setVisibility(View.VISIBLE);
//                takeScreenshot(view.getRootView());
                new UiUpdate().execute(v);
            }
        });

        mBadgeRelativeLayout = (RelativeLayout) view.findViewById(R.id.mini_details_badge_relative_layout);
        mDetailsRelativeLayout = (RelativeLayout) view.findViewById(R.id.mini_details_relative_layout);


        mHourlyViewPager = (ViewPager) view.findViewById(R.id.hourly_view_pager);
        mDailyViewPager = (ViewPager) view.findViewById(R.id.daily_view_pager);

        mHourlyPagerAdapter = new HourlyPagerAdapter(getContext(), mWeather);
        mHourlyViewPager.setAdapter(mHourlyPagerAdapter);

        mDailyPagerAdapter = new DailyPagerAdapter(getContext(), mWeather);
        mDailyViewPager.setAdapter(mDailyPagerAdapter);

        return view;
    }

    private class UiUpdate extends AsyncTask<View, Integer, View> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mBadgeRelativeLayout.setVisibility(View.VISIBLE);
            mShareImageView.setVisibility(View.GONE);
        }

        @Override
        protected void onPostExecute(View v) {
            super.onPostExecute(v);
            takeScreenshot(mDetailsRelativeLayout.getRootView());
        }

        @Override
        protected View doInBackground(View... params) {
            return null;
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    private void takeScreenshot(View v) {
        //Bitmap bitmap;
        v.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(v.getDrawingCache());
        v.setDrawingCacheEnabled(false);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File f = new File(Environment.getExternalStorageDirectory() + File.separator + "share.jpg");
        try {
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mBadgeRelativeLayout.setVisibility(View.GONE);
        getDialog().dismiss();
        shareBitmap(bitmap);

    }

    private void shareBitmap(Bitmap bitmap) {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/jpeg");
//        ContentValues values = new ContentValues();
//        values.put(MediaStore.Images.Media.TITLE, "Current weather for...");
//        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
//        Uri uri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
//
//        OutputStream outputStream;
//        try {
//            outputStream = getActivity().getContentResolver().openOutputStream(uri);
//            outputStream.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        share.putExtra(Intent.EXTRA_STREAM, Uri.parse(Environment.getExternalStorageDirectory() +
        File.separator + "share.jpg"));
        startActivity(Intent.createChooser(share, getString(R.string.intent_share_text)));
    }
}
