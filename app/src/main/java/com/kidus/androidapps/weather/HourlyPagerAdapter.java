package com.kidus.androidapps.weather;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Locale;

/**
 * Created by Kidus on .
 */
public class HourlyPagerAdapter  extends PagerAdapter {
    private Context mContext;
    private Weather mWeather;
    private LayoutInflater mInflater;

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        TextView hourlyTitleTextView, hourlySubtitleTextView;
        ImageView hourlyIcon;

        position++;//get the previous hour out of the list

        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = mInflater.inflate(R.layout.daily_hourly_items, container, false);

        hourlySubtitleTextView = (TextView) itemView.findViewById(R.id.daily_hourly_subtitle_text_view);
        hourlyTitleTextView = (TextView) itemView.findViewById(R.id.daily_hourly_title_text_view);
        hourlyIcon = (ImageView) itemView.findViewById(R.id.daily_hourly_icon_image_view);

        WeatherData hourlyWeather = mWeather.getHourlyWeatherData().get(position);

        if (Locale.getDefault().getLanguage().equals("am")) {
            int hr = Integer.valueOf(hourlyWeather.getFormattedDate("h"));
            hr = hourlyWeather.convertToEthHour(hr);
            String hour = String.format("%d:%s", hr, hourlyWeather.getFormattedDate("mm a"));
            hourlyTitleTextView.setText(hour);
        } else {
            hourlyTitleTextView.setText(hourlyWeather.getFormattedDate("h:mm a"));
        }
        hourlySubtitleTextView.setText(String.valueOf((int) hourlyWeather.getTemperature()) + "°");
        Picasso.with(mContext).load(hourlyWeather.getIconResourceId()).into(hourlyIcon);


        container.addView(itemView);

        return itemView;
    }

    @Override
    public int getCount() {
        return mWeather.getHourlyWeatherData().size() - 24;//only give 24h
    }

    @Override
    public float getPageWidth(int position) {
        return 1/3f;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

    public HourlyPagerAdapter(Context context, Weather weather) {
        mContext = context;
        mWeather = weather;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (RelativeLayout) object;

    }
}
