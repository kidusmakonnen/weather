package com.kidus.androidapps.weather;

import android.content.Context;
import android.location.Geocoder;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Kidus on .
 */
public class City implements Serializable {
    private Context mContext;
    private LocationName mName;
    private LocationName mCountryName;
    private double mLongitude;
    private double mLatitude;
    private int mDayImageResourceId;
    private int mNightImageResourceId;
    private boolean mIsDefaultCity;
    private String mPlaceId;
    private String mPhotoLink;
    private String mPhotoReference;
    private boolean mPhotoLinkFetched = false;

    public City(LocationName name, LocationName countryName, double longitude, double latitude, int dayImageResourceId, int nightImageResourceId) {
        mName = name;
        mCountryName = countryName;
        mLongitude = longitude;
        mLatitude = latitude;
        mDayImageResourceId = dayImageResourceId;
        mNightImageResourceId = nightImageResourceId;
        mIsDefaultCity = false;
    }

    public boolean isPhotoLinkFetched() {
        return mPhotoLinkFetched;
    }

    public String getPhotoLink() {
        return mPhotoLink;
    }

    public City(LocationName name, LocationName countryName, double longitude, double latitude, String placeId, Context context) {
        this(name, countryName, longitude, latitude, R.drawable.default_city, R.drawable.default_city);
        mPlaceId = placeId;
        mContext = context;
        String req = "https://maps.googleapis.com/maps/api/place/details/json?placeid=%s&key=AIzaSyB3wnTNRKL_tcJPtpCr3kJmbAieQvJ9uig";
        req = String.format(req, mPlaceId);
        StringRequest stringRequest = new StringRequest(req, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONObject object = JSONParse.toJSONObject(response);
                object = JSONParse.toJSONObject(JSONParse.get(object, "result").toString());
                JSONArray jsonArray = JSONParse.getJSONArray(object, "photos");
                object = JSONParse.getJSONObject(jsonArray, 0);
                mPhotoReference = JSONParse.get(object, "photo_reference").toString();

                mPhotoLink = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=480&photoreference=%s&key=AIzaSyA0MDcW6YkieATKoFajGbUClug65mPoGJg";
                mPhotoLink = String.format(mPhotoLink, mPhotoReference);
                mPhotoLinkFetched = true;
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        requestQueue.add(stringRequest);
    }

    public String getPlaceId() {
        return mPlaceId;
    }

    public boolean isDefaultCity() {
        return mIsDefaultCity;
    }

    public void setIsDefaultCity(boolean isDefaultCity) {
        mIsDefaultCity = isDefaultCity;
    }

    public String getName() {
        return mName.toString();
    }

    public void setName(LocationName name) {
        mName = name;
    }

    public void setCountryName(LocationName countryName) {
        mCountryName = countryName;
    }

    public String getCountryName() {
        return mCountryName.toString();
    }


    public double getLongitude() {
        return mLongitude;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public int getDayImageResourceId() {
        return mDayImageResourceId;
    }

    public int getNightImageResourceId() {
        return mNightImageResourceId;
    }
}
