package com.kidus.androidapps.weather;

import android.content.Context;

import java.util.Locale;

/**
 * Created by Kidus on .
 */
public class WeatherDataDaily extends WeatherData {

    private long mSunriseTime, mSunsetTime;
    private long mTemperatureMinTime, mTemperatureMaxTime;
    private long mApparentTemperatureMinTime, mApparentTemperatureMaxTime;
    private double mMoonPhase;
    private double mPrecipIntensityMax;
    private double mTemperatureMin, mTemperatureMax;
    private double mApparentTemperatureMin, mApparentTemperatureMax;

    public WeatherDataDaily(Context context, long time, WeatherIcon icon, String summary,
                            double precipitationIntensity, double precipitationProbability,
                            String precipType, double dewPoint,
                            double humidity, double windSpeed, double windBearing, double cloudCover, double pressure,
                            double ozone, long sunriseTime, long sunsetTime, long temperatureMinTime,
                            long temperatureMaxTime, long apparentTemperatureMinTime,
                            long apparentTemperatureMaxTime, double moonPhase,
                            double precipIntensityMax, double temperatureMin, double temperatureMax,
                            double apparentTemperatureMin, double apparentTemperatureMax) {

        super(context, time, icon, summary, precipitationIntensity, precipitationProbability, precipType, 0,
                0, dewPoint, humidity, windSpeed, windBearing, cloudCover, pressure, ozone);
        mSunriseTime = sunriseTime;
        mSunsetTime = sunsetTime;
        mTemperatureMinTime = temperatureMinTime;
        mTemperatureMaxTime = temperatureMaxTime;
        mApparentTemperatureMinTime = apparentTemperatureMinTime;
        mApparentTemperatureMaxTime = apparentTemperatureMaxTime;
        mMoonPhase = moonPhase;
        mPrecipIntensityMax = precipIntensityMax;
        mTemperatureMin = temperatureMin;
        mTemperatureMax = temperatureMax;
        mApparentTemperatureMin = apparentTemperatureMin;
        mApparentTemperatureMax = apparentTemperatureMax;
    }

    @Override
    public String toString() {
        return String.format("Date: %s\nSummary: %s\nTemperature min: %s\nTemperature max: %s",
                getFormattedDate(getTime()), getSummary(), getTemperatureMin(), getTemperatureMax());
    }

    public long getSunriseTime() {
        return mSunriseTime;
    }

    public long getSunsetTime() {
        return mSunsetTime;
    }

    public long getTemperatureMinTime() {
        return mTemperatureMinTime;
    }

    public long getTemperatureMaxTime() {
        return mTemperatureMaxTime;
    }

    public long getApparentTemperatureMinTime() {
        return mApparentTemperatureMinTime;
    }

    public long getApparentTemperatureMaxTime() {
        return mApparentTemperatureMaxTime;
    }

    public double getMoonPhase() {
        return mMoonPhase;
    }

    public double getPrecipIntensityMax() {
        return mPrecipIntensityMax;
    }

    public double getTemperatureMin() {
        return mTemperatureMin;
    }

    public double getTemperatureMax() {
        return mTemperatureMax;
    }

    public double getApparentTemperatureMin() {
        return mApparentTemperatureMin;
    }

    public double getApparentTemperatureMax() {
        return mApparentTemperatureMax;
    }

    @Override
    public String getLocalizedSummary() {
        if (Locale.getDefault().getLanguage().equals("am")) {
            String msg = "ከፍተኛው %d ዝቅተኛው %d %s ፣ ከፍተኛው የዕለቱ ሙቀት %s %d ሰዓት ላይ እንዲሁም ዝቅተኛው ሙቀት %s %d ሰዓት ላይ ይሆናል።\n";
            msg = String.format(msg, (int) getTemperatureMax(), (int) getTemperatureMin(), super.getLocalizedSummary(),
                   getFormattedDate(getTemperatureMaxTime(), "a"), convertToEthHour(Integer.parseInt(getFormattedDate(getTemperatureMaxTime(), "h"))),
                   getFormattedDate(getTemperatureMinTime(), "a"), convertToEthHour(Integer.parseInt(getFormattedDate(getTemperatureMinTime(), "h")))
            );
            return msg;
        } else {
            return super.getLocalizedSummary();
        }
    }
}
