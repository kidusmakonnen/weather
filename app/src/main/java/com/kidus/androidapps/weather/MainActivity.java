package com.kidus.androidapps.weather;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static int defaultCity;
    private String mRequestString;
    private TextView mCityNameTextView;
    private TextView mTemperatureLargeTextView, mHiTextView, mLoTextView;
    private TextView mSummaryTextView;
    private TextView mDetail1TextView, mDetail2TextView, mDetail3TextView;
    private ImageView mIconImageView;
    private ImageView mCityImageView;
    private RelativeLayout mLoadingRelativeLayout;
    private TextView mLoadingTextView;
    private ProgressBar mLoadingProgressBar;
    private Button mUpdateButton;
    private EditText mOptionsEditText;
    private JSONObject mJSONObject;
    private ArrayList<String> mJsonResponses;
    private int requestCount = 0;
    private boolean loadFromPrefsCalled = false;
    //    private TenDayForecastAdapter mTenDayForecastAdapter;
    private CityListWeatherAdapter mCityListWeatherAdapter;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;

    private ImageView mWeatherAnimationImageView;
    private ImageView mMenuImageView;

    private ArrayList<Weather> mWeathers = new ArrayList<>();

    private SlidingMenu menu;


//    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences preferences = getSharedPreferences("WEATHER_DATA", MODE_PRIVATE);
        defaultCity = preferences.getInt("DEFAULT_CITY", 0);


        mCityNameTextView = (TextView) findViewById(R.id.city_name_text_view);
        mTemperatureLargeTextView = (TextView) findViewById(R.id.temperature_current_text_view);
        mHiTextView = (TextView) findViewById(R.id.temperature_hi_text_view);
        mLoTextView = (TextView) findViewById(R.id.temperature_lo_text_view);
        mSummaryTextView = (TextView) findViewById(R.id.current_weather_summary_text_view);
        mDetail1TextView = (TextView) findViewById(R.id.detail1_label_text_view);
        mDetail2TextView = (TextView) findViewById(R.id.detail2_label_text_view);
        mDetail3TextView = (TextView) findViewById(R.id.detail3_label_text_view);
        mLoadingRelativeLayout = (RelativeLayout) findViewById(R.id.loading_linear_layout);
        mLoadingTextView = (TextView) findViewById(R.id.loading_message_text_view);
        mWeatherAnimationImageView = (ImageView) findViewById(R.id.weather_animation);
        mCityImageView = (ImageView) findViewById(R.id.city_picture_image_view);
        mMenuImageView = (ImageView) findViewById(R.id.menu_image_view);
        mLoadingProgressBar = (ProgressBar) findViewById(R.id.loading_progress_bar);

        mLoadingRelativeLayout.setVisibility(View.VISIBLE);

        mWeathers = WeatherList.sWeatherArrayList;
        mLoadingProgressBar.setMax(mWeathers.size());

        if (defaultCity < mWeathers.size()) {
            setDefaultCity(defaultCity);
        } else {
            defaultCity = 0;
            setDefaultCity(defaultCity);
        }

//        prepare(mWeathers.get(0), 0, mWeathers.size() + 1);
        prepare(mWeathers);

        mMenuImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menu.showMenu();
            }
        });

        menu = new SlidingMenu(this);
        menu.setMode(SlidingMenu.LEFT);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
        menu.setShadowWidthRes(R.dimen.shadow_width);
        menu.setShadowDrawable(R.drawable.shadow);
        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        menu.setFadeDegree(0.35f);
        menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        menu.setMenu(R.layout.settings_sliding_menu);
//        for (int i = 0; i < mWeathers.size(); i++) {
//            prepare(mWeathers.get(i));
//        }
//
//        int i = 0;
//        requestSuccessful = true;
//        while (i < mWeathers.size()) {
//            if (requestSuccessful) {
//                requestSuccessful = false;
//                prepare(mWeathers.get(i));
//                i++;
//            } else {
//
//            }
//        }


    }

    private void setDefaultCity(int indexOfCity) {
        WeatherList.clearDefaultCity();
        WeatherList.sWeatherArrayList.get(indexOfCity).getCity().setIsDefaultCity(true);
    }


    private void prepareUi(ArrayList<Weather> w, int defaultCityIndex) {
        int i = defaultCityIndex;
        prepareDefaultCity(w.get(i), w.get(i).getCityName(), w.get(i).getCity().getDayImageResourceId());
        prepareCityListRecyclerView();
        saveWeatherData();
    }

    private void prepareDefaultCity(Weather weather, String cityName, int cityImageResourceId) {
        String currentTemperature = String.valueOf((int) weather.getCurrentWeatherData().getTemperature());
        String todayHiTemperature = String.format("%d", (int) weather.getDailyWeatherData().get(0).getTemperatureMax());
        String todayLoTemperature = String.format("%d", (int) weather.getDailyWeatherData().get(0).getTemperatureMin());
        String summary = weather.getCurrentWeatherData().getSummary();
        String detail1 = String.format("%s: %.2f%%", getString(R.string.ui_cloud_cover), weather.getCurrentWeatherData().getCloudCover() * 100);
        String detail2 = String.format("%s: %.2f%s", getString(R.string.ui_wind_speed), weather.getCurrentWeatherData().getWindSpeed(), getString(R.string.ui_unit_mps));
        String detail3 = String.format("%s: %s", getString(R.string.ui_precip_prob), getString(weather.getCurrentWeatherData().getPrecipProbabilityString()));
        mCityNameTextView.setText(cityName);
        mTemperatureLargeTextView.setText(currentTemperature);
        mHiTextView.setText(todayHiTemperature);
        mLoTextView.setText(todayLoTemperature);
        mSummaryTextView.setText(summary);
        mDetail1TextView.setText(detail1);
        mDetail2TextView.setText(detail2);
        mDetail3TextView.setText(detail3);
        if (weather.getCity().getPlaceId() != null && weather.getCity().isPhotoLinkFetched()) {
            Picasso.with(getApplicationContext())
                    .load(weather.getCity().getPhotoLink())
                    .placeholder(cityImageResourceId)
                    .into(mCityImageView);
        } else {
            Picasso.with(getApplicationContext())
                    .load(cityImageResourceId)
                    .into(mCityImageView);
        }
        startDefaultCityAnimation(weather);
        SharedPreferences.Editor editor = getSharedPreferences("WEATHER_DATA", MODE_PRIVATE).edit();
        editor.putInt("DEFAULT_CITY", WeatherList.getDefaultCityIndex());
        editor.commit();
    }

    public void prepareDefaultCity() {
        Weather w = WeatherList.getDefaultCityWeather();
        prepareDefaultCity(w, w.getCityName(), w.getCity().getDayImageResourceId());
        if (menu.isMenuShowing()) {
            menu.showContent();
        }
    }

    public void prepareOneCity(final City c) {
        double longitude = c.getLongitude();
        double latitude = c.getLatitude();
        mRequestString = "https://api.forecast.io/forecast/a6bb9e0a1d1904c896ef68d6a2d142ef/%f,%f?units=ca";
        mRequestString = String.format(mRequestString, longitude, latitude);
        final ProgressDialog d = new ProgressDialog(this);
        d.setTitle("Adding...");
        StringRequest stringRequest = new StringRequest(mRequestString, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Weather w = new Weather(null, c);
                w.prepareFromJSON(response);
                WeatherList.sWeatherArrayList.add(w);
                mCityListWeatherAdapter.notifyItemInserted(WeatherList.getSize() - 1);
                d.dismiss();
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        RequestQueue rq = Volley.newRequestQueue(this);
        rq.add(stringRequest);
        saveWeatherData();
    }

    public void notifyRemoved(int deleteNotifyIndex) {
        mCityListWeatherAdapter.notifyItemRemoved(deleteNotifyIndex);
        mCityListWeatherAdapter.notifyItemRangeChanged(deleteNotifyIndex, WeatherList.getSize());
    }

    public void notifyDatasetChanged() {
        mCityListWeatherAdapter.notifyDataSetChanged();
    }

    public void prepare(final ArrayList<Weather> weathers) {
        prepare(weathers, false);
    }
    public void prepare(final ArrayList<Weather> weathers, final boolean refresh) {
        final ProgressDialog d = new ProgressDialog(this);
        d.setTitle(getString(R.string.dialog_refreshing));
        if (refresh) {
            requestCount = 0;
            d.show();
        }
        for (int j = 0; j < weathers.size(); j++) {
            final int i = j;
            double longitude = weathers.get(i).getCity().getLongitude();
            double latitude = weathers.get(i).getCity().getLatitude();
            mRequestString = "https://api.forecast.io/forecast/a6bb9e0a1d1904c896ef68d6a2d142ef/%f,%f?units=ca";
            mRequestString = String.format(mRequestString, longitude, latitude);
            StringRequest stringRequest = new StringRequest(mRequestString, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Weather weather = new Weather(MainActivity.this, weathers.get(i).getCity());
                    weather.prepareFromJSON(response);
                    mWeathers.set(mWeathers.indexOf(weathers.get(i)), weather);
                    requestCount++;
                    if (!refresh) {
                        mLoadingTextView.setText(String.format("Loading %d of %d", requestCount, weathers.size()));
                        mLoadingProgressBar.setProgress(requestCount);
                    }
                    if (requestCount == weathers.size()) {
                        prepareUi(mWeathers, defaultCity);
                        if (refresh) {
                            d.dismiss();
                            if (menu.isMenuShowing()) {
                                menu.showContent();
                            }
                            Toast.makeText(MainActivity.this, getString(R.string.message_updated), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (!loadFromPrefsCalled) {
                                prepareFromPrefs();
                                loadFromPrefsCalled = true;
                            }
                        }
                    }
            );
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }
    }

    private void prepareCityListRecyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.ten_day_recycler_view);
        mLinearLayoutManager = new LinearLayoutManager(MainActivity.this);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        mCityListWeatherAdapter = new CityListWeatherAdapter(MainActivity.this, mWeathers);
        mCityListWeatherAdapter.setListener(new CityListWeatherAdapter.Listener() {
            @Override
            public void onClick(int position) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                MiniDetailsDialogFragment miniDetailsFragment = new MiniDetailsDialogFragment();

                Bundle bundle = new Bundle();
                bundle.putSerializable("WEATHER", mWeathers.get(position));

                miniDetailsFragment.setArguments(bundle);
                miniDetailsFragment.show(fragmentManager, "Details");
//                Toast.makeText(MainActivity.this, mWeathers.get(position).getCityName(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(final int position, View v) {
                final PopupMenu popupMenu = new PopupMenu(MainActivity.this, v);
                popupMenu.getMenuInflater().inflate(R.menu.options_menu, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.set_default_city:
                                defaultCity = position;
                                setDefaultCity(defaultCity);
                                Weather w = mWeathers.get(position);
                                prepareDefaultCity(w, w.getCityName(), w.getCity().getDayImageResourceId());
                                break;
                            case R.id.delete_city_menu:
                                WeatherList.sWeatherArrayList.remove(position);
                                mCityListWeatherAdapter.notifyItemRemoved(position);
                                mCityListWeatherAdapter.notifyItemRangeChanged(position, WeatherList.getSize());
//                                mCityListWeatherAdapter.notifyDataSetChanged();
                        }

                        return true;
                    }
                });
                popupMenu.show();
            }
        });
        mRecyclerView.setAdapter(mCityListWeatherAdapter);
        mLoadingRelativeLayout.setVisibility(View.GONE);


        //saveWeatherData();
    }

    private void prepareFromPrefs() {
        SharedPreferences preferences = getSharedPreferences("WEATHER_DATA", MODE_PRIVATE);
        Gson gson = new Gson();
        ArrayList<String> responses = new ArrayList<>();
        String s = preferences.getString("WEATHER_LIST", null);
        if (s != null) {
            responses = gson.fromJson(s, new TypeToken<ArrayList<String>>() {
            }.getType());
            if (responses.size() == mWeathers.size()) {
                mLoadingTextView.setText("Loading last saved info...");
                for (int i = 0; i < mWeathers.size(); i++) {
                    mWeathers.get(i).prepareFromJSON(responses.get(i));
                }
                prepareUi(mWeathers, defaultCity);
                return;
            }
        }
        mLoadingTextView.setText("Unable to connect :(");
        Toast.makeText(MainActivity.this, getString(R.string.error_unable_to_connect), Toast.LENGTH_LONG).show();
    }

    private void startDefaultCityAnimation(Weather weather) {
        if (mWeatherAnimationImageView.getDrawable() != null) {
            if (((AnimationDrawable) mWeatherAnimationImageView.getDrawable()).isRunning()) {
                //((AnimationDrawable) mWeatherAnimationImageView.getDrawable()).stop();
                //AnimationDrawable animationDrawable = ((AnimationDrawable) mWeatherAnimationImageView.getDrawable());
                //((AnimationDrawable) mWeatherAnimationImageView.getDrawable()).setVisible(false, true);
                mWeatherAnimationImageView.getDrawable().setVisible(false, true);

//                ((AnimationDrawable) mWeatherAnimationImageView.getDrawable()).selectDrawable(0);
//                mWeatherAnimationImageView.setImageDrawable(null);
//                for (int i = 0; i < animationDrawable.getNumberOfFrames(); i++){
//                    Drawable d = animationDrawable.getFrame(i);
//                    if (d instanceof BitmapDrawable) {
//                        ((BitmapDrawable) d).getBitmap().recycle();
//                    }
//                    d.setCallback(null);
//                }
                //animationDrawable.setCallback(null);
            }
            //mWeatherAnimationImageView.clearAnimation();
        }
        mWeatherAnimationImageView.setImageResource(weather.getCurrentWeatherData().getAnimationResourceId());
        ((AnimationDrawable) mWeatherAnimationImageView.getDrawable()).start();
    }

    private void setupMenu(View v) {
        PopupMenu popupMenu = new PopupMenu(MainActivity.this, v);
        popupMenu.getMenuInflater().inflate(R.menu.about_menu, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.about_menu:
//                        new AlertDialog.Builder(MainActivity.this)
//                                .setTitle(getString(R.string.menu_about_label))
//                                .setMessage(getString(R.string.menu_about_text))
//                                .setPositiveButton(android.R.string.ok,
//                                        new DialogInterface.OnClickListener() {
//                                            @Override
//                                            public void onClick(DialogInterface dialog, int which) {
//
//                                            }
//                                        })
//                                .show();
                        break;
                }
                return false;
            }
        });
        popupMenu.show();
    }

    private void saveWeatherData() {
        SharedPreferences.Editor editor = getSharedPreferences("WEATHER_DATA", MODE_PRIVATE).edit();
        Gson gson = new Gson();
        String weatherList = gson.toJson(WeatherList.getAllJsonResponse());
        editor.putString("WEATHER_LIST", weatherList);
        editor.putInt("DEFAULT_CITY", defaultCity);
        editor.commit();
    }

    @Override
    public void onBackPressed() {
        if (menu.isMenuShowing()) {
            menu.showContent();
        } else {
            super.onBackPressed();
        }
    }
}