package com.kidus.androidapps.weather;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by Kidus on .
 */
public class SettingsListFragment extends ListFragment {

    String mAddress, mLat, mLong, mPlaceId;
    ArrayList<CheckBox> mCheckBoxes;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list, null);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        switch (position) {
            case 0:
                setDefaultCity();
                break;
            case 1:
                addCity();
                break;
            case 2:
                removeCity();
                break;
            case 3:
                updateWeatherInfo();
                break;
            case 4:
                showAboutDialog();
                break;
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        SettingsAdapter adapter = new SettingsAdapter(getActivity());
        adapter.add(new SettingsItem(R.string.menu_change_default_city, R.drawable.ic_favorite));
        adapter.add(new SettingsItem(R.string.menu_add_city, R.drawable.ic_add));
//        adapter.add(new SettingsItem("Add City From My Location", R.drawable.ic_location));
        adapter.add(new SettingsItem(R.string.menu_remove_city, R.drawable.ic_remove));
        adapter.add(new SettingsItem(R.string.menu_refresh, R.drawable.ic_refresh));
        adapter.add(new SettingsItem(R.string.menu_about, R.drawable.ic_about));
//        adapter.add(new SettingsItem(R.string.menu_help, R.drawable.ic_action_name));
//        adapter.add(new SettingsItem(R.string.menu_share, R.drawable.ic_share));
        setListAdapter(adapter);
    }

    private int dpToPx(int dpResourceId) {
        return (int) getResources().getDimension(dpResourceId);
    }

    private void addCity() {
        LinearLayout searchBoxLinearLayout = (LinearLayout) getLayoutInflater(null).inflate(R.layout.search_city_layout, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext())
                .setTitle(getString(R.string.alert_add_city))
                .setView(searchBoxLinearLayout)
                .setCancelable(true)
                .setPositiveButton(getString(android.R.string.ok),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                LocationName cityName = LocationName.get(mAddress.split(",")[0]);
                                LocationName countryName = LocationName.get(mAddress.split(",")[mAddress.split(",").length - 1]);
                                double lat = Double.valueOf(mLat);
                                double lng = Double.valueOf(mLong);
                                City c = new City(cityName, countryName, lat, lng, mPlaceId, getContext());
                                CityList.addToCityList(c);

                                ((MainActivity) getActivity()).prepareOneCity(c);
                            }
                        });
        AlertDialog dialog = builder.create();
        dialog.show();

        final EditText cityNameEditText = (EditText) dialog.findViewById(R.id.alert_city_search_edit_text);

        Button searchCityButton = (Button) dialog.findViewById(R.id.alert_city_search_button);
        final CheckBox resultCheckBox = (CheckBox) dialog.findViewById(R.id.alert_city_result_check_box);

        final ProgressDialog d = new ProgressDialog(getContext());
        d.setTitle(getString(R.string.dialog_fetching));
        searchCityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String req = "http://maps.googleapis.com/maps/api/geocode/json?address=%s&sensor=false&language=am";
                try {
                    req = String.format(req, URLEncoder.encode(cityNameEditText.getText().toString(), "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                d.show();
                StringRequest stringRequest = new StringRequest(req, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject object = JSONParse.toJSONObject(response);
                        if (JSONParse.get(object, "status").toString().equals("OK")) {
                            JSONArray jsonArray = JSONParse.getJSONArray(object, "results");
                            object = JSONParse.getJSONObject(jsonArray, 0);

                            mAddress = JSONParse.get(object, "formatted_address").toString();
                            mPlaceId = JSONParse.get(object, "place_id").toString();

                            object = JSONParse.toJSONObject(JSONParse.get(object, "geometry").toString());
                            object = JSONParse.toJSONObject(JSONParse.get(object, "location").toString());

                            mLat = JSONParse.get(object, "lat").toString();
                            mLong = JSONParse.get(object, "lng").toString();

                            resultCheckBox.setText(mAddress);
                            resultCheckBox.setVisibility(View.VISIBLE);
                        } else {
                            Toast.makeText(getContext(), getString(R.string.generic_error), Toast.LENGTH_SHORT).show();
                        }
                        d.dismiss();

                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getContext(), getString(R.string.generic_error), Toast.LENGTH_SHORT).show();
                            }
                        });
                RequestQueue rq = Volley.newRequestQueue(getContext());
                rq.add(stringRequest);
            }
        });


    }
    private void setDefaultCity() {
        LinearLayout linearLayout = new LinearLayout(getContext());
        ScrollView scrollView = new ScrollView(getContext());
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setPadding(dpToPx(R.dimen.alert_padding), dpToPx(R.dimen.alert_padding), dpToPx(R.dimen.alert_padding), dpToPx(R.dimen.alert_padding));
        final RadioGroup radioGroup = new RadioGroup(getContext());
        radioGroup.setOrientation(RadioGroup.VERTICAL);
        for (int i = 0; i < WeatherList.getSize(); i++) {
            String name = String.format("%s, %s", WeatherList.get(i).getCityName(), WeatherList.get(i).getCityCountry());
            RadioButton radioButton = new RadioButton(getContext());
            radioButton.setText(name);
            radioButton.setId(i);
            radioGroup.addView(radioButton);
        }
        RadioButton rb = (RadioButton) radioGroup.getChildAt(WeatherList.getDefaultCityIndex());
        rb.setChecked(true);
        linearLayout.addView(radioGroup);
        scrollView.addView(linearLayout);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext())
                .setTitle(getString(R.string.alert_set_default))
                .setView(scrollView)
                .setCancelable(true)
                .setPositiveButton(getString(android.R.string.ok),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                WeatherList.clearDefaultCity();
                                WeatherList.setDefaultCity(getSelectedRadioButtonIndex(radioGroup));
                                ((MainActivity) getActivity()).prepareDefaultCity();
                            }
                        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    private void removeCity() {
        LinearLayout linearLayout = new LinearLayout(getContext());
        ScrollView scrollView = new ScrollView(getContext());
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setPadding(dpToPx(R.dimen.alert_padding), dpToPx(R.dimen.alert_padding), dpToPx(R.dimen.alert_padding), dpToPx(R.dimen.alert_padding));
        final ArrayList<Integer> deleteList = new ArrayList<>();
        mCheckBoxes = new ArrayList<>();
        for (int i = 0; i < WeatherList.getSize(); i++) {
            String name = String.format("%s, %s", WeatherList.get(i).getCityName(), WeatherList.get(i).getCityCountry());
            CheckBox checkBox = new CheckBox(getContext());
            checkBox.setText(name);
            checkBox.setId(i);
            mCheckBoxes.add(checkBox);
//            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    if (isChecked) {
//                        deleteList.add(mSelectedCity);
//                    } else {
//                        deleteList.remove(deleteList.indexOf(mSelectedCity));
//                    }
//                }
//            });
            linearLayout.addView(checkBox);

        }
        scrollView.addView(linearLayout);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext())
                .setTitle(getString(R.string.alert_delete))
                .setView(scrollView)
                .setCancelable(true)
                .setPositiveButton(getString(android.R.string.ok),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                for (int i = 0; i < mCheckBoxes.size(); i++) {
                                    if (mCheckBoxes.get(i).isChecked()) {
                                        deleteList.add(mCheckBoxes.get(i).getId());
//                                        mCheckBoxes.remove(i);
                                    }
                                }
                                int iter = 0;
                                for (int j : deleteList) {
                                    int i = j;
                                    if (iter != 0) {
                                        i = i - iter;
                                    }
                                    WeatherList.sWeatherArrayList.remove(i);
                                    ((MainActivity) getActivity()).notifyRemoved(i);
                                    iter++;
                                }
                            }
                        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    private void updateWeatherInfo() {
        ((MainActivity) getActivity()).prepare(WeatherList.sWeatherArrayList, true);
    }
    private void showAboutDialog() {
        ScrollView scrollView = new ScrollView(getActivity());

        LinearLayout linearLayout = new LinearLayout(getActivity());
        linearLayout.setPadding(dpToPx(R.dimen.alert_padding), dpToPx(R.dimen.alert_padding), dpToPx(R.dimen.alert_padding), dpToPx(R.dimen.alert_padding));
        TextView textView = new TextView(getActivity());
        textView.setText(Html.fromHtml(getString(R.string.menu_about_html)));
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.addView(textView);
        scrollView.addView(linearLayout);
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext())
                .setView(scrollView)
                .setTitle("About")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private int getSelectedRadioButtonIndex(RadioGroup rg) {
        return rg.getCheckedRadioButtonId();
//        int id = rg.getCheckedRadioButtonId();
//        int weatherSize = WeatherList.getSize();
//        if (id == -1) {
//            return 0;
//        } else {
//            if (id % weatherSize == 0) {
//                return weatherSize - 1;
//            } else {
//                return (id % weatherSize) - 1;
//            }
//            RadioButton temp = (RadioButton) getActivity().findViewById(id);
//            return rg.indexOfChild(temp);
//        }
    }

    private class SettingsItem {
        private String mText;
        private int mIconResId;

        public SettingsItem(String text, int iconResId) {
            mText = text;
            mIconResId = iconResId;
        }

        public SettingsItem(int textResId, int iconResId) {
            this(getString(textResId), iconResId);
        }
    }

    public class SettingsAdapter extends ArrayAdapter<SettingsItem> {
        public SettingsAdapter(Context context) {
            super(context, 0);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.row, null);
            }
            ImageView icon = (ImageView) convertView.findViewById(R.id.row_icon);
            icon.setImageResource(getItem(position).mIconResId);
            TextView title = (TextView) convertView.findViewById(R.id.row_title);
            title.setText(getItem(position).mText);
            return convertView;
        }
    }
}
