package com.kidus.androidapps.weather;

import android.content.Context;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Kidus on .
 */
public class WeatherData implements Serializable {
    private long mTime;
    private WeatherIcon mIcon;
    private String mSummary;
    private double mPrecipitationIntensity;
    private double mPrecipitationProbability;
    private String mPrecipitationType;
    private double mTemperature;
    private double mApparentTemperature;
    private double mDewPoint;
    private double mHumidity;
    private double mWindSpeed;
    private double mWindBearing;
    private double mCloudCover;
    private double mPressure;
    private double mOzone;

    private Context mContext;
    private int mIconResourceId;

    public WeatherData(long time, WeatherIcon icon, String summary, double temperature) {
        mTime = time;
        mIcon = icon;
        mSummary = summary;
        mTemperature = temperature;
    }

    public WeatherData(Context context, long time, WeatherIcon icon, String summary, double precipitationIntensity,
                       double precipitationProbability, String precipitationType, double temperature,
                       double apparentTemperature, double dewPoint, double humidity,
                       double windSpeed, double windBearing, double cloudCover, double pressure, double ozone) {
        mContext = context;
        mTime = time;
        mIcon = icon;
        mSummary = summary;
        mPrecipitationIntensity = precipitationIntensity;
        mPrecipitationProbability = precipitationProbability;
        mPrecipitationType = precipitationType;
        mTemperature = temperature;
        mApparentTemperature = apparentTemperature;
        mDewPoint = dewPoint;
        mHumidity = humidity;
        mWindSpeed = windSpeed;
        mWindBearing = windBearing;
        mCloudCover = cloudCover;
        mPressure = pressure;
        mOzone = ozone;
    }

    protected Date dateFromUnixTimestamp(long unixTimestamp) {
        return new Date(unixTimestamp * 1000);
    }

    protected String twelveHourTimeFromUnixTimestamp(long unixTimestamp) {
        DateFormat dateFormat = new SimpleDateFormat("h:mm a");
        return dateFormat.format(dateFromUnixTimestamp(unixTimestamp));
    }

    protected String getFormattedDate(long unixTimestamp, String format) {
        DateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(dateFromUnixTimestamp(unixTimestamp));
    }

    protected String getFormattedDate(long unixTimestamp) {
        return getFormattedDate(unixTimestamp, "EE MMMM d, y h:mm a");
    }

    protected String getFormattedDate() {
        return getFormattedDate(getTime(), "EE MMMM d, y h:mm a");
    }

    protected String getFormattedDate(String format) {
        return getFormattedDate(getTime(), format);
    }

    public int getAnimationResourceId() {
        switch (getIcon()) {
            case CLEAR_DAY:
                return R.drawable.sunny_animation;
//            case CLEAR_NIGHT:
//                return R.drawable.weezle_fullmoon;
            case RAIN:
                return R.drawable.light_rain_animation;
//            case SNOW:
//                return R.drawable.weezle_snow;
//            case SLEET:
//                return R.drawable.weezle_sun_medium_ice;
//            case WIND:
//                return R.drawable.weezle_fog;
            case CLOUDY:
                return R.drawable.cloudy_animation;
            case PARTLY_CLOUDY_DAY:
                return R.drawable.cloudy_day_animation;
//            case PARTLY_CLOUDY_NIGHT:
//                return R.drawable.weezle_moon_cloud_medium;
//            case HAIL:
//                return R.drawable.weezle_medium_ice;
            case THUNDERSTORM:
                return R.drawable.thunderstorm_animation;
//            case TORNADO:
//                return R.drawable.weezle_cloud_thunder_rain;
            default:
                return R.drawable.cloudy_day_animation;
        }
    }

    public int getIconResourceId() {
        switch (getIcon()) {
            case CLEAR_DAY:
                return R.drawable.weezle_sun;
            case CLEAR_NIGHT:
                return R.drawable.weezle_fullmoon;
            case RAIN:
                return R.drawable.weezle_rain;
            case SNOW:
                return R.drawable.weezle_snow;
            case SLEET:
                return R.drawable.weezle_sun_medium_ice;
            case WIND:
                return R.drawable.weezle_fog;
            case CLOUDY:
                return R.drawable.weezle_max_cloud;
            case PARTLY_CLOUDY_DAY:
                return R.drawable.weezle_sun_maximum_clouds;
            case PARTLY_CLOUDY_NIGHT:
                return R.drawable.weezle_moon_cloud_medium;
            case HAIL:
                return R.drawable.weezle_medium_ice;
            case THUNDERSTORM:
                return R.drawable.weezle_cloud_thunder_rain;
            case TORNADO:
                return R.drawable.weezle_cloud_thunder_rain;
            default:
                return R.drawable.weezle_sun;
        }
    }

    public long getTime() {
        return mTime;
    }

    public void setTime(long time) {
        mTime = time;
    }

    public WeatherIcon getIcon() {
        return mIcon;
    }

    public void setIcon(WeatherIcon icon) {
        mIcon = icon;
    }

    public String getSummary() {
        return getLocalizedSummary();
    }

    public void setSummary(String summary) {
        mSummary = summary;
    }

    public String getLocalizedSummary() {
        if (Locale.getDefault().getLanguage().equals("am")) {
            switch (getIcon()) {
                case CLOUDY:
                    return "ደመናማ";
                case PARTLY_CLOUDY_DAY:
                case PARTLY_CLOUDY_NIGHT:
                    return "ከፊል ደመናማ";
                case CLEAR_DAY:
                    return "ፀሐያማ";
                case RAIN:
                    return "ዝናባማ";
                case CLEAR_NIGHT:
                    return "ግልጽ ሠማይ";
                case HAIL:
                    return "በረዶአማ";
                case THUNDERSTORM:
                    return "ነጐድጓዳማ";
                case TORNADO:
                    return "አውሎ ንፋስ";
                case WIND:
                    return "ነፋሻማ";
                default:
                    return mSummary;
            }
        } else {
            return mSummary;
        }
    }

    public double getPrecipitationIntensity() {
        return mPrecipitationIntensity;
    }

    public void setPrecipitationIntensity(double precipitationIntensity) {
        mPrecipitationIntensity = precipitationIntensity;
    }

    public double getPrecipitationProbability() {
        return mPrecipitationProbability;
    }

    public void setPrecipitationProbability(double precipitationProbability) {
        mPrecipitationProbability = precipitationProbability;
    }

    public double getTemperature() {
        return mTemperature;
    }

    public void setTemperature(double temperature) {
        mTemperature = temperature;
    }

    public double getApparentTemperature() {
        return mApparentTemperature;
    }

    public void setApparentTemperature(double apparentTemperature) {
        mApparentTemperature = apparentTemperature;
    }

    public double getDewPoint() {
        return mDewPoint;
    }

    public void setDewPoint(double dewPoint) {
        mDewPoint = dewPoint;
    }

    public double getHumidity() {
        return mHumidity;
    }

    public void setHumidity(double humidity) {
        mHumidity = humidity;
    }

    public double getWindSpeed() {
        return mWindSpeed;
    }

    public void setWindSpeed(double windSpeed) {
        mWindSpeed = windSpeed;
    }

    public double getCloudCover() {
        return mCloudCover;
    }

    public void setCloudCover(double cloudCover) {
        mCloudCover = cloudCover;
    }

    public int getPrecipProbabilityString() {
        double prob = getPrecipitationProbability();
        if (prob == 0.0) {
            return R.string.none;
        } else if (prob < 0.5) {
            return R.string.low;
        } else if (prob == 0.5) {
            return R.string.mid;
        } else {
            return R.string.high;
        }
    }

    public int convertToEthHour(int x) {
        int etHr = ((x + 6) % 12);
        if (etHr == 0) {
            return 12;
        } else {
            return etHr;
        }
    }

    public double getPressure() {
        return mPressure;
    }

    public void setPressure(double pressure) {
        mPressure = pressure;
    }

    public double getOzone() {
        return mOzone;
    }

    public void setOzone(double ozone) {
        mOzone = ozone;
    }

    @Override
    public String toString() {
        return String.format("Date: %s\nSummary: %s\nTemperature: %s", getFormattedDate(getTime()),
                getSummary(), getTemperature());
    }

    public enum WeatherIcon {
        CLEAR_DAY, CLEAR_NIGHT, RAIN, SNOW, SLEET, WIND, FOG, CLOUDY, PARTLY_CLOUDY_DAY,
        PARTLY_CLOUDY_NIGHT, HAIL, THUNDERSTORM, TORNADO
    }
}
