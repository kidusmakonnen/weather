package com.kidus.androidapps.weather;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Kidus on .
 */
public class WeatherList implements Serializable {
    public static ArrayList<Weather> sWeatherArrayList = new ArrayList<>();
    public static Date sLastUpdated;

    static {
        sWeatherArrayList = Weather.prepareWeatherListFromCityArray(CityList.sCities);
        setUpdateDate();
    }

    public static void setUpdateDate() {
        sLastUpdated = new Date();
    }

    public static void addToWeatherListFromCity(City city) {
        sWeatherArrayList.add(new Weather(null, city));
    }

    public static ArrayList<String> getAllJsonResponse() {
        ArrayList<String> strings = new ArrayList<>();
        for (int i = 0; i < sWeatherArrayList.size(); i++) {
            strings.add(sWeatherArrayList.get(i).getJsonResponse());
        }
        return strings;
    }

    public static void clearDefaultCity() {
        for (Weather i : sWeatherArrayList) {
            i.getCity().setIsDefaultCity(false);
        }
    }

    public static int getDefaultCityIndex() {
        for (int i = 0; i < sWeatherArrayList.size(); i++) {
            if (sWeatherArrayList.get(i).getCity().isDefaultCity()) {
                return i;
            }
        }
        return 0;
    }

    public static void setDefaultCity(int i) {
        clearDefaultCity();
        sWeatherArrayList.get(i).getCity().setIsDefaultCity(true);
    }

    public static Weather getDefaultCityWeather() {
        for (int i = 0; i < sWeatherArrayList.size(); i++) {
            if (sWeatherArrayList.get(i).getCity().isDefaultCity()) {
                return sWeatherArrayList.get(i);
            }
        }
        return sWeatherArrayList.get(0);
    }

    public static Weather get(int index) {
        return sWeatherArrayList.get(index);
    }

    public static int getSize() {
        return sWeatherArrayList.size();
    }
}
