package com.kidus.androidapps.weather;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Kidus on .
 */
public class LocationName {
    private ArrayList<String> names;

    public LocationName() {
        names = new ArrayList<>();
    }

    public static LocationName get(String... strings) {
        LocationName temp = new LocationName();
        for (String i : strings) {
            temp.names.add(i);
        }

        return temp;
    }

    @Override
    public String toString() {
        if(Locale.getDefault().getLanguage().equals("am"))
        {
            if (names.size() > 1) {
                return names.get(1);
            } else {
                return names.get(0);
            }
        } else {
            return names.get(0);
        }
    }
}
